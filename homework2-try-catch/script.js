//теория 
// когда работаем с получение данных с сервера
// кагда работаем с даннымы, которые вводит пользователь
// 

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const props = ["author", "name", "price"];
const root = document.createElement("div");
const list = document.createElement("ul");

root.id = "root";
document.body.append(root);
root.append(list);

const newBooks = books.filter((item) => {
  for (let i of props) {
    try {
      if (!item.hasOwnProperty(i)) {
        throw new Error(`отсутствует ${i}`);
      }
    } catch (error) {
      console.log(error);
      return
    }
  }
  return item;
});

for (let elem of newBooks) {
  const listBook = document.createElement("li");
  for (let key in elem) {
    const p = document.createElement("p");
    p.textContent = `${key} : ${elem[key]}`;
    listBook.append(p);
  }

  list.append(listBook);
}
