const btn = document.querySelector(".btn");
btn.addEventListener("click", getIp);

async function getIp() {
  const res = await fetch("https://api.ipify.org/?format=json");
  const jsonData = await res.json();
  const { ip } = jsonData;

  const resInfo = await fetch(
    `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
  );
  const jsonInfo = await resInfo.json();

  const { continent, country, regionName, city, district } = await jsonInfo;
//   console.log(continent, country, regionName, city, district);

  const continentEl = document.querySelector(".continent");
  const countryEl = document.querySelector(".country");
  const regionNameEl = document.querySelector(".regionName");
  const cityEl = document.querySelector(".city");
  const districtEl = document.querySelector(".district");

  continentEl.textContent = continent;
  countryEl.textContent = country;
  regionNameEl.textContent = regionName;
  cityEl.textContent = city;
  if (district === "") {
    districtEl.textContent = "район не найден";
  } else {
    districtEl.textContent = district;
  }
}
