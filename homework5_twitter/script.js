const wrapper = document.querySelector(".wrapper");

let firstUserName;
let firstUserUserName;
let firstUserEmail;
let firstUserPostId;
let postId = 101;

const inputTitle = document.createElement("input");
const inputText = document.createElement("input");
function createForm() {
  const wrapForm = document.createElement("div");
  wrapForm.classList.add("wrapForm");
  wrapForm.classList.add("hide");
  const form = document.createElement("form");
  form.classList.add("form");
  form.id = "formElem";

  inputTitle.classList.add("inputTitle");
  inputTitle.setAttribute("placeholder", "введите заголовок");
  inputTitle.setAttribute("name", "title");
  inputTitle.type = "text";

  inputText.classList.add("inputText");
  inputText.setAttribute("placeholder", "введите текст");
  inputText.setAttribute("name", "text");
  inputText.type = "text";

  const inputSubmit = document.createElement("input");
  inputSubmit.classList.add("inputSubmit");
  inputSubmit.type = "submit";
  inputSubmit.addEventListener("click", (event) => {
    event.preventDefault();
    addPost();
    openForm();
  });

  const closeIconWrapForm = document.createElement("div");
  closeIconWrapForm.classList.add("closeIconWrapForm");
  const closeIconForm = document.createElement("img");
  closeIconForm.setAttribute("src", "./icon-close.png");
  closeIconWrapForm.append(closeIconForm);

  closeIconWrapForm.addEventListener("click", () => {
    openForm();
  });

  form.append(inputTitle, inputText, inputSubmit, closeIconWrapForm);
  wrapForm.append(form);
  document.body.prepend(wrapForm);
}
createForm();

function openForm() {
  const form = document.querySelector(".wrapForm");
  form.classList.toggle("hide");
}

let usersArr;

const wrapAddBtn = document.createElement("div");
wrapAddBtn.classList.add("wrapAddBtn");

const addPostBtn = document.createElement("button");
addPostBtn.textContent = "Добавить публикацию";
addPostBtn.classList.add("button-29");
addPostBtn.addEventListener("click", openForm);
wrapAddBtn.append(addPostBtn);
document.body.prepend(wrapAddBtn);

const spinnerWrap = document.createElement("div");
spinnerWrap.classList.add("spinnerWrap");
const spinner = document.createElement("img");
spinner.classList.add("spinner");
spinner.setAttribute("src", "./spinner.svg");
spinnerWrap.append(spinner);
wrapper.prepend(spinnerWrap);

async function getUsers() {
  try {
    const response = await fetch("https://ajax.test-danit.com/api/json/users");
    const users = response.json();
    users.then((data) => {
      usersArr = data;
      const first = data[0];
      firstUserName = first.name;
      firstUserUserName = first.username;
      firstUserEmail = first.email;
    });
  } catch (err) {
    console.error(err);
  }
}

getUsers();

fetch("https://ajax.test-danit.com/api/json/posts")
  .then((response) => response.json())
  .then((data) => {
    data.forEach((elem) => {
      let name;
      let userName;
      let email;

      let userId = elem.userId;
      usersArr.forEach((item) => {
        if (userId === item.id) {
          name = item.name;
          userName = item.username;
          email = item.email;
        }
      });

      new Card(
        elem.title,
        elem.body,
        userName,
        name,
        email,
        elem.id,
        wrapper
      ).render();
    });
  })
  .catch((err) => {
    spinnerWrap.style.display = "none";
    const error = document.createElement("div");
    error.textContent = "Что-то пошло не так";
    wrapper.prepend(error);
    console.log(err);
  });

class Card {
  constructor(title, text, userName, name, email, postId, parent) {
    this.title = title;
    this.text = text;
    this.name = name;
    this.userName = userName;
    this.email = email;
    this.postId = postId;
    this.parent = parent;
  }

  render() {
    const post = document.createElement("div");
    post.classList.add("post");

    const wrapPost = document.createElement("div");
    wrapPost.classList.add("wrapPost");

    const usersDataWrap = document.createElement("div");
    usersDataWrap.classList.add("usersDataWrap");

    const nameSurname = document.createElement("span");
    nameSurname.classList.add("nameSurname");
    nameSurname.textContent = this.name;

    const userName = document.createElement("span");
    userName.classList.add("userName");
    userName.textContent = this.userName;

    const userEmail = document.createElement("div");
    userEmail.classList.add("userEmail");
    userEmail.textContent = this.email;

    usersDataWrap.append(nameSurname, userName, userEmail);
    wrapPost.append(usersDataWrap);

    const titleContent = document.createElement("div");
    titleContent.classList.add("titleContent");
    titleContent.textContent = this.title;
    wrapPost.append(titleContent);

    const contentText = document.createElement("div");
    contentText.classList.add("contentText");
    contentText.textContent = this.text;
    wrapPost.append(contentText);

    const closeIconWrap = document.createElement("div");
    closeIconWrap.classList.add("closeIconWrap");
    const closeIcon = document.createElement("img");
    closeIcon.setAttribute("src", "./icon-close.png");
    closeIconWrap.append(closeIcon);

    closeIconWrap.addEventListener("click", (event) => {
      let e = event.currentTarget.parentNode;
      deletePost(e, this.postId);
    });

    const editIconWrap = document.createElement("div");
    editIconWrap.classList.add("editIconWrap");
    const editIcon = document.createElement("img");
    editIcon.setAttribute("src", "./icon-edit.png");
    editIconWrap.classList.add("editIconWrap");
    editIconWrap.append(editIcon);

    const wrapEdit = document.createElement("div");
    wrapEdit.id = `edit${this.postId}`;
    wrapEdit.classList.add("wrapEdit");
    wrapEdit.classList.add("hideEdit");
    const editTitle = document.createElement("textarea");
    editTitle.value = titleContent.textContent;
    const editText = document.createElement("textarea");
    editText.value = contentText.textContent;
    const inputSaveEdit = document.createElement("input");
    inputSaveEdit.type = "submit";
    wrapEdit.append(editTitle, editText, inputSaveEdit);

    editIconWrap.addEventListener("click", (event) => {
      let a = event.currentTarget.parentNode;
      setTimeout(() => {
        Array.from(a.children)[
          Array.from(a.children).length - 1
        ].classList.toggle("hideEdit");
      }, 50);
      // let e = event.currentTarget.parentNode;
      wrapPost.append(wrapEdit);
    });

    inputSaveEdit.addEventListener("click", () => {
      wrapEdit.classList.toggle("hideEdit");
      editPost(this.postId, editTitle.value, editText.value);
    });
    spinnerWrap.style.display = "none";

    wrapPost.append(closeIconWrap);
    wrapPost.append(editIconWrap);
    post.append(wrapPost);
    this.parent.prepend(post);
  }
}

function addPost() {
  let inputTitle = document.querySelector(".inputTitle").value;
  let inputText = document.querySelector(".inputText").value;

  fetch(`https://ajax.test-danit.com/api/json/posts`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title: inputTitle,
      text: inputText,
    }),
  })
    .then((res) => res.json())
    .then(({ title, text }) => {
      new Card(
        title,
        text,
        firstUserUserName,
        firstUserName,
        firstUserEmail,
        postId,
        wrapper
      ).render();
      postId++;
    })
    .catch((err) => console.log(err));
}

function deletePost(event, id) {
  if (id > 100) {
    event.parentNode.remove();
    return;
  }
  fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (response.ok === true) {
        event.parentNode.remove();
      }
    })
    .catch((err) => console.log(err));
}

function editPost(id, editTitle, editText) {
  if (id > 100) {
    let ef = document.querySelector(`#edit${id}`);
    ef.parentNode.children[1].textContent = Array.from(ef.children)[0].value;
    ef.parentNode.children[2].textContent = Array.from(ef.children)[1].value;
    return;
  }
  fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      title: editTitle,
      text: editText,
    }),
  })
    .then((response) => {
      if (response.ok === true) {
        let ef = document.querySelector(`#edit${id}`);
        ef.parentNode.children[1].textContent = Array.from(
          ef.children
        )[0].value;
        ef.parentNode.children[2].textContent = Array.from(
          ef.children
        )[1].value;
      }
      return response.json();
    })
    .catch((err) => console.log(err));
}
