// ## Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// это технология общения с сервером без перезагрузки страницы.


const root = document.createElement("div");
document.body.append(root);

function renderFilm(title, number, summery) {
  const wrapFilm = document.createElement("div");
  const titleFIlm = document.createElement("h2");
  const numberFilm = document.createElement("p");
  const summeryFilm = document.createElement("p");

  wrapFilm.style.cssText = `
    border: 1px solid grey;
    border-radius: 5px;
    margin:40px;
    padding: 10px;
    `;

  titleFIlm.textContent = title;
  numberFilm.textContent = `Епізод ${number}`;
  summeryFilm.textContent = summery;

  wrapFilm.append(titleFIlm);
  wrapFilm.append(numberFilm);
  wrapFilm.append(summeryFilm);


    let spinner = document.createElement('img');
    spinner.classList.add('spinner')
    spinner.setAttribute('src', './spinner.svg');
    titleFIlm.after(spinner)
  
  root.append(wrapFilm);
}

let arrId = [];

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((data) => {
    data.forEach((item) => {
      renderFilm(item.name, item.episodeId, item.openingCrawl);

      let arrIdEp = item.characters.map((character) => {
        let id = character.split("/")[6];
        return id;
      });
      arrId.push(arrIdEp);
    });
    return arrId;
  })
  .then((arrId) => {
    fetch("https://ajax.test-danit.com/api/swapi/people")
    
      .then((res) => res.json())
      .then((characters) => {

        function renderCharacters() {

          const spinners = document.querySelectorAll('.spinner');
          spinners.forEach(spinner => {
            spinner.style.display = 'none';
          })
          const h2 = document.querySelectorAll("h2");
          let a = "";
          let namesArr = arrId.map((elemArrId) => {
            let names = elemArrId.map((id) => {
              characters.forEach((char) => {
                if (char.id === Number(id)) {
                  a = char.name;
                }
              });
              return a;
            });
            return names;
          });
          //-----------------------------------
          let count = 0;
          h2.forEach((title) => {
            const ul = document.createElement("ul");
            namesArr[count].forEach((name) => {
              const li = document.createElement("li");
              li.textContent = name;
              ul.append(li);
            });
            title.after(ul);
            count++;
          });
        }
        renderCharacters();
      });
  });






