// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// прототипне наслідування - это возможность создавать новые объекты на основе уже существующего, использовать его свойства и методы.
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//super() - функция вызывающая родительский конструктор.



class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this.name = value;
    }
    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }

    set salary(value) {
        this._name = value;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee{

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
    return this._salary * 3;
}
    
}

const first = new Programmer('Vasiliy', 27, 10000, ['js', 'java']);

const second = new Programmer('Alex', 45, 7000, ['python', 'java']);

const third = new Programmer('Ann', 238, 8000, ['php', 'c++']);

console.log(first);
console.log(second);
console.log(third);
console.log(first.salary);
console.log(second.salary);
console.log(third.salary);

